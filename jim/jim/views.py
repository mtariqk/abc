from django.shortcuts import render_to_response
from django.template import RequestContext

def home(request):
	ctx=RequestContext(request)
	temp_name="index.html"
	lang=['python','shruti','java','php']
	return render_to_response(temp_name, locals(),ctx)

def my(request):
	ctx=RequestContext(request)
	temp_name="my.html"
	lang=['python','tariq','java','php']
	return render_to_response(temp_name, locals(),ctx)